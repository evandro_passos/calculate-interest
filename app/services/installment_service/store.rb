module InstallmentService
  class Store
    def self.call(present_value, number_of_installments, monthly_interest_rate)

      installment_value = TablePrice.calculate(present_value, number_of_installments, monthly_interest_rate)

      installment = Installment.new({
        present_value: present_value,
        number_of_installments: number_of_installments,
        monthly_interest_rate: monthly_interest_rate,
        installment_value: installment_value
      })
      installment.save!

      installment
    end
  end
end
