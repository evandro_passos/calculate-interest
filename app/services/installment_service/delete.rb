module InstallmentService
  class Delete
    def self.call(id)
      if id.blank?
        raise InvalidArgumentError, "id params is required"
      end

      Installment.where(id: id).delete_all
    end
  end
end
