module InstallmentService
  class GetById
    def self.call(id)
      if id.blank?
        raise InvalidArgumentError, "id params is required"
      end

      Installment.find(id)
    end
  end
end
