class Api::V1::InstallmentController < ApplicationController

  def create
    installment = InstallmentService::Store.call(params[:present_value].to_f,
      params[:number_of_installments].to_f, params[:monthly_interest_rate].to_f)

    render json: {
      id: installment.id,
      installment_value: installment.installment_value
    }, status: 200
  rescue InvalidArgumentError, ActiveRecord::RecordInvalid => e
    render json: { message: e.message }, status: 400
  end

  def show
    installment = InstallmentService::GetById.call(params[:id])

    render json: {
      id: installment.id,
      present_value: installment.present_value,
      number_of_installments: installment.number_of_installments,
      monthly_interest_rate: installment.monthly_interest_rate,
      installment_value: installment.installment_value
    }, status: 200
  rescue InvalidArgumentError, ActiveRecord::RecordNotFound => e
    render json: { message: e.message }, status: 400
  end

  def destroy
    records_affected = InstallmentService::Delete.call(params[:id])

    render json: { removed: (records_affected > 0)}, status: 200
  rescue InvalidArgumentError => e
    render json: { message: e.message }, status: 400
  end
end
