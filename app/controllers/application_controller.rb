class ApplicationController < ActionController::API

  rescue_from Exception do |e|
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    render json: { message: 'unexpected error occurred' }, status: 500
  end
end
