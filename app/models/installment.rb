class Installment < ApplicationRecord
  validates :present_value, :number_of_installments, :monthly_interest_rate, :installment_value, presence: true
end
