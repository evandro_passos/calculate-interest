class TablePrice
  def self.calculate(present_value, number_of_installments, monthly_interest_rate)
    if present_value.blank?
      raise InvalidArgumentError, "present_value params is required"
    end

    if number_of_installments.blank?
      raise InvalidArgumentError, "number_of_installments params is required"
    end

    if monthly_interest_rate.blank?
      raise InvalidArgumentError, "monthly_interest_rate params is required"
    end

    (present_value * ((((1 + monthly_interest_rate) ** number_of_installments) * monthly_interest_rate)/(((1 + monthly_interest_rate) ** number_of_installments) - 1))).round(2)
  end
end
