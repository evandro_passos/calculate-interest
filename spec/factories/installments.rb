FactoryBot.define do
  factory :installment do
    id { SecureRandom.uuid }
    present_value { Faker::Number.decimal(4, 2) }
    number_of_installments { Faker::Number.between(1, 12) }
    monthly_interest_rate { Faker::Number.between(5, 100) / 100 }
    installment_value { Faker::Number.decimal(3) }
  end
end
