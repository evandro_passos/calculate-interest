require 'rails_helper'

RSpec.describe Installment, type: :model do
  it { expect(create(:installment)).to be_valid }

  it { should validate_presence_of(:present_value) }
  it { should validate_presence_of(:number_of_installments) }
  it { should validate_presence_of(:monthly_interest_rate) }
  it { should validate_presence_of(:installment_value) }
end
