require 'rails_helper'

RSpec.describe TablePrice, type: :model do
  describe '::calculate' do
    let(:present_value) { 1000 }
    let(:number_of_installments) { 4 }
    let(:monthly_interest_rate) { 0.03 }


    it 'when present_value params is blank' do
      expect {
        TablePrice.calculate(nil, number_of_installments, monthly_interest_rate)
      }.to raise_error(InvalidArgumentError, "present_value params is required")
    end

    it 'when number_of_installments params is blank' do
      expect {
        TablePrice.calculate(present_value, nil, monthly_interest_rate)
      }.to raise_error(InvalidArgumentError, "number_of_installments params is required")
    end

    it 'when number_of_installments params is blank' do
      expect {
        TablePrice.calculate(present_value, number_of_installments, nil)
      }.to raise_error(InvalidArgumentError, "monthly_interest_rate params is required")
    end

    it 'when installment_value is calculated' do
      installment_value = TablePrice.calculate(present_value, number_of_installments, monthly_interest_rate)
      expect(installment_value).to eq(269.03)
    end
  end
end
