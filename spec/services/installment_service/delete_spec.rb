RSpec.describe InstallmentService::Delete do
  describe '::call' do
    it 'when id params is blank' do
      expect {
        described_class.call(nil)
      }.to raise_error(InvalidArgumentError, "id params is required")
    end

    it 'when id not found' do
      affecteds = described_class.call('guid-not-exists')

      expect(affecteds).to eq(0)
    end

    it 'when record is deleted' do
      installment = create(:installment)
      affecteds = described_class.call(installment.id)

      expect(affecteds).to eq(1)
    end
  end
end
