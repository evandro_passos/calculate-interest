RSpec.describe InstallmentService::GetById do
  describe '::call' do
    it 'when id params is blank' do
      expect {
        described_class.call(nil)
      }.to raise_error(InvalidArgumentError, "id params is required")
    end

    it 'when id not found' do
      expect {
      described_class.call('guid-not-exists')
    }.to raise_error(ActiveRecord::RecordNotFound)

    end

    it 'when record exists' do
      installment = create(:installment)
      installment_returned = described_class.call(installment.id)

      expect(installment.id).to eq(installment_returned.id)
    end
  end
end
