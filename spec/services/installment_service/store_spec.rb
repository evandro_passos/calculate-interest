RSpec.describe InstallmentService::Store do
  describe '::call' do
    let(:present_value) { 1000 }
    let(:number_of_installments) { 4 }
    let(:monthly_interest_rate) { 0.03 }

    it 'TablePrice.calculate function is called' do
      allow(TablePrice)
        .to receive(:calculate).and_return(269.03)

      described_class.call(present_value, number_of_installments, monthly_interest_rate)
    end

    it 'installment saved' do
      installment = described_class.call(present_value, number_of_installments, monthly_interest_rate)


      expect(Installment.count).to eq(1)
      expect(installment.installment_value).to eq(269.03)
    end
  end
end
