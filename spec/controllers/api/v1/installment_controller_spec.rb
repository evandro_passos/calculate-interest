require 'rails_helper'

RSpec.describe Api::V1::InstallmentController, type: :controller do

  describe "POST #create" do
    let(:json_send) {
      {
        present_value: 1000.00,
        number_of_installments: 4,
        monthly_interest_rate: 0.03
      }
    }

    it "when argument is invalid" do
      allow(InstallmentService::Store)
        .to receive(:call).and_raise(InvalidArgumentError)

      post :create, params: json_send

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 400
    end

    it "when save model ocurred error" do
      allow(InstallmentService::Store)
        .to receive(:call).and_raise(ActiveRecord::RecordInvalid)

      post :create, params: json_send

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 400
    end

    it "when save installment success" do

      post :create, params: json_send

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 200
      expect(json_response["installment_value"]).to eq(269.03)
    end
  end

  describe "GET #show" do
    it "when argument is invalid" do
      allow(InstallmentService::GetById)
        .to receive(:call).and_raise(InvalidArgumentError)

      get :show, params: { id: 1 }

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 400
    end

    it "when id not exists" do
      get :show, params: { id: 1 }

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 400
    end

    it "when installment is returned" do
      installment = create(:installment)
      get :show, params: { id: installment.id }

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 200
      expect(json_response["installment_value"]).to eq(installment.installment_value)
    end
  end

  describe "DELETE #destroy" do
    it "when id not exists" do
      delete :destroy, params: { id: 1 }

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 200
      expect(json_response["removed"]).to eq(false)
    end

    it "when installment is returned" do
      installment = create(:installment)
      delete :destroy, params: { id: installment.id }

      json_response = JSON.parse(response.body)
      expect(response).to have_http_status 200
      expect(json_response["removed"]).to eq(true)
    end
  end
end
