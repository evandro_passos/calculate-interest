# Calculate Interest

> Api calculates the value of the parcel using TablePrice

## Run with Docker

### Dependencies

* `docker > 18`
* `docker-compose > 1.20`

### Setting up the project

Run `./setup.sh`

### Starting the project

Run `./start.sh`

### Project URL

`CalculateInterest` runs on `localhost:3000`.

### Endpoints

Calculate installment `POST localhost:3000/api/v1/installment`
Payload example:
```json
{
    "present_value": 1000.00,
    "number_of_installments": 4,
    "monthly_interest_rate": 0.03
}
```

Return installment `GET localhost:3000/api/v1/installment/:id`
Response example:
```json
{
    "id": "B62C624E-A805-11E5-8C4A-50E44124FF49",
    "present_value": 1000.00,
    "number_of_installments": 4,
    "monthly_interest_rate": 0.03
}
```

Delete installment `DELETE localhost:3000/api/v1/installment/:id`
