FROM ruby:2.6.3-alpine3.9

ENV RAILS_LOG_TO_STDOUT true
ENV LANG C.UTF-8

WORKDIR /app

COPY Gemfile .
COPY Gemfile.lock .

RUN gem install bundler:2.0.2 && \
    apk update && \
    apk add --no-cache build-base libxslt-dev g++ gcc libxml2-dev make postgresql-dev tzdata git && \
    bundle install && \
    rm -r /var/cache/apk/*

COPY . .

EXPOSE 3000

CMD ["./server.sh"]
